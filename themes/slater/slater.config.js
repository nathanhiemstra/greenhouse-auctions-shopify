const path = require('path')

module.exports = {
  themes: {
    development: {
      id: '104831320216',
      password: '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',
      store: 'greenhouse-auctions.myshopify.com',
      ignore: [
        'settings_data.json' // leave this here to avoid overriding theme settings on sync
      ]
    }
  }
}
